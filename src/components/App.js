import React from 'react';
import PaneHeroes from './heroes/PaneHeroes';
import 'semantic-ui-css/semantic.min.css'
import SideBar from './SideBar';
import { Container, Grid } from 'semantic-ui-react';
import ListHeroes from './heroes/ListHeroes';



var quests = [
    {
        id: 1,
        name: 'Meet the Hobbits',
        begin: '',
        end: '',
        heroes: [],
        status: ''
    }
]



class App extends React.Component {
    render() {
        return (
            <Container>
                <Grid columns={2} padded='vertically'>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <SideBar />
                        </Grid.Column>
                        <Grid.Column width={13}>
                            <PaneHeroes />
                            <ListHeroes />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        );
    }
}

export default App;