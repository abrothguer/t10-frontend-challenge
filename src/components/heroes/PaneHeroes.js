import React from 'react';
import { Tab } from 'semantic-ui-react';
import OverallHeroes from './OverallHeroes';
import ListHeroes from './ListHeroes';
import AddHero from './AddHero';

const panes = [
    {
        menuItem: 'Overall',
        render: () => (
            <Tab.Pane attached = {false}>
                <OverallHeroes />
            </Tab.Pane>
        )
    },
    {
        menuItem: 'Heróis',
        render: () => (
            <Tab.Pane attached = {false}>
                <ListHeroes />
            </Tab.Pane>
        )
    },
    {
        menuItem: 'Adiconar Herói',
        render: () => (
            <Tab.Pane attached = {false}>
                <AddHero />
            </Tab.Pane>
        )
    }
]

class PaneHeroes extends React.Component {

    render () {
        return (
            <Tab menu = {{ secondary: true, pointing: true }}
                panes = { panes }
            />
        )
    }
}

export default PaneHeroes;