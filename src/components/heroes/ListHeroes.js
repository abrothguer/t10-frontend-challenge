import React from 'react';
import { Segment, Grid, Divider, Header, List } from 'semantic-ui-react';

var heroes = [
    {
        id: 1,
        name: 'Aragorn',
        xp: 1024
    },
    {
        id: 2,
        name: 'Gimli',
        xp: 512
    },
    {
        id: 3,
        name: 'Legolas',
        xp: 513
    },
    {
        id: 4,
        name: 'Gandalf',
        xp: 1000000
    }
]

const heroesList = heroes.map((hero) => {
    return (
        <Segment>
            <Grid columns={2} divided>
                <Grid.Row verticalAlign="middle">
                    <Grid.Column>
                        <Header>
                            {hero.name}
                        </Header>
                        {hero.xp}
                    </Grid.Column>
                    <Grid.Column>
                        <List bulleted>
                            <List.Item>#1q</List.Item>
                            <List.Item>#2q</List.Item>
                            <List.Item>#3q</List.Item>
                            <List.Item>#3q</List.Item>
                            <List.Item>#3q</List.Item>
                        </List>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    )
});

class ListHeroes extends React.Component {

    render() {
        return (
            <div>
               {heroesList }
            </div>
        );
    }
}

export default ListHeroes;