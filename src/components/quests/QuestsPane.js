import React from 'react';
import { Tab } from 'semantic-ui-react';
import OverallQuests from './OverallQuests';
import AddQuest from './AddQuest';

const panes = [
    {
        menuItem: 'Overall',
        render: () => (
            <Tab.Pane attached = {false}>
                <OverallQuests />
            </Tab.Pane>
        )
    },
    {
        menuItem: 'Criar Quest',
        render: () => (
            <Tab.Pane attached = {false}>
                <AddQuest />
            </Tab.Pane>
        )
    }
]

class PaneQuests extends React.Component {

    render () {
        return (
            <Tab menu = {{ secondary: true, pointing: true }}
                panes = { panes }
            />
        )
    }
}

export default PaneQuests;