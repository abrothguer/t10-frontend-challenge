import React from 'react';
import { Menu, Icon } from 'semantic-ui-react'

class SideBar extends React.Component {

    state = { activeItem: 'heroes' }

    render() {
        return (
            <Menu pointing secondary vertical>
                <Menu.Item
                    name='heróis'
                    active={this.state.activeItem === 'heroes'}
                    onClick={this.handleItemClick}
                >
                    <Icon name='user plus' />
                    Heróis
                </Menu.Item>
                <Menu.Item
                    name='quests'
                    active={this.state.activeItem === 'quests'}
                    onClick={this.handleItemClick}
                >
                    <Icon name='map signs' />
                    Quests
                </Menu.Item>
            </Menu>
        );
    }
}

export default SideBar;